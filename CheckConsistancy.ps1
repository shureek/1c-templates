$Services = Get-Service -Include 'hasplms'
$Services | ?{ $_.Status -eq 'Running'} | Stop-Service

# Clear registry
$Folders = 'HKLM:\SYSTEM\CurrentControlSet',
           'HKLM:\SYSTEM\CurrentControlSet001',
           'HKLM:\SYSTEM\CurrentControlSet002'

$Keys = 'Services\haspflt',
        'Services\Emulator',
        'Services\Emu',
        'NEWHASP',
        'MultiKey',
        'Services\vusbbus'

foreach ($Name in $Keys) {
    foreach ($Folder in $Folders) {
        Get-Item -Path "$Folder\$Name" -ErrorAction SilentlyContinue | Remove-Item -Recurse
    }
}

# Clear files
gci multikey.* -Recurse -ErrorAction SilentlyContinue | Remove-Item
